﻿using BookStoreProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStoreProject.Controllers
{
    public class HomeController : Controller
    {
        bookstoreEntities2 db = new bookstoreEntities2();
        public ActionResult Index()
        {
            var item = db.tblBooks.ToList();
            
            return View(item);
        }

        public ActionResult Navbar() {
            var items = db.tblCategories.ToList();
            return PartialView("~/Views/Shared/_Navbar.cshtml", items);
        }

        public ActionResult Category(int category_id) {
            var items = db.tblBooks.Where(x => x.catId == category_id).ToList();
            return View("index",items);
        }

        [HttpGet]
        public ActionResult Search() {
            var title = Request.QueryString["search"];
            var items = db.tblBooks.Where(x => x.Book_title.Contains(title)).ToList();
            
            return View("Index", items);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult ViewBook(int id)
        {
            var book_id= id;
            //var query = db.tblBooks.Where(x => x.Book_id == book_id).Join;
            var query = from book in db.tblBooks join category in db.tblCategories on book.catId equals category.catId             
                        select new MyViewModel{ Book = book, Category = category};

            var item = query.Where(x => x.Book.Book_id == book_id).First();

            item.Book.Book_countview += 1;
            db.SaveChanges();
            return View(item);
        }


    }
}
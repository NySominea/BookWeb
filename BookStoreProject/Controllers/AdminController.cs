﻿using BookStoreProject.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace BookStoreProject.Controllers
{
    public class AdminController : Controller
    {
        bookstoreEntities2 db = new bookstoreEntities2();

        // GET: Admin
        public ActionResult Index()
        {
            return RedirectToAction("Dashboard");
        }
        public ActionResult InsertBook()
        {
            var items = db.tblCategories.ToList();
            return View(items);
        }

        [HttpPost]
        public ActionResult AddBook()
        {
            int cat_id = int.Parse(Request.Form["category"]);
            string title = Request.Form["title"];
            string author = Request.Form["author"];
            string year = Request.Form["publish_year"].ToString();
            string description = Request.Form["description"];
            string imageName = "";
            string bookName = "";

            HttpPostedFileBase image = Request.Files["image"];

                if (image.ContentLength > 0)
                {
                    imageName = Path.GetFileName(image.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Image"), imageName);
                    image.SaveAs(path);
                }

            HttpPostedFileBase book = Request.Files["book"];

            if (book.ContentLength > 0)
            {
                bookName = Path.GetFileName(book.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/Book"), bookName);
                book.SaveAs(path);
            }


            var newBook = new tblBook();
            newBook.Book_title = title;
            newBook.Book_author = author;
            newBook.Book_des = description;
            newBook.Book_publish_year = year;
            newBook.catId = cat_id;
            newBook.Book_cover1 = imageName;
            newBook.Book_cover2 = bookName;
            newBook.Book_countview = 0;
            newBook.Book_dateInsert = DateTime.Now.ToString();

            db.tblBooks.Add(newBook);
            db.SaveChanges();

            return RedirectToAction("ViewBook");
        }

        public ActionResult ViewBook()
        {
            var query = from book in db.tblBooks
                        join category in db.tblCategories on book.catId equals category.catId
                        select new MyViewModel { Book = book, Category = category };
            var items = query.ToList();
            return View(items);
        }

        

        public ActionResult EditBook(int id) {
            var book = db.tblBooks.Where(x => x.Book_id == id).First();
            var category = db.tblCategories.ToList();
            var items = new MyViewModel { Book = book, Categories = category };
            return View(items);
        }

        [HttpPost]
        public ActionResult UpdateBook(int id) {
            int cat_id = int.Parse(Request.Form["category"]);
            string title = Request.Form["title"];
            string author = Request.Form["author"];
            string year = Request.Form["year"].ToString();
            string description = Request.Form["description"];
            string imageName = "";
            string bookName = "";

            HttpPostedFileBase image = Request.Files["image"];

            if (image!=null && image.ContentLength > 0)
            {
                imageName = Path.GetFileName(image.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/Image"), imageName);
                image.SaveAs(path);
            }

            HttpPostedFileBase book = Request.Files["book"];

            if (book!=null && book.ContentLength > 0)
            {
                bookName = Path.GetFileName(book.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/Book"), bookName);
                book.SaveAs(path);
            }


            var newBook = db.tblBooks.Where(x => x.Book_id == id).First();
            newBook.Book_title = title;
            newBook.Book_author = author;
            newBook.Book_des = description;
            newBook.Book_publish_year = year;
            newBook.catId = cat_id;
            if(imageName!="") newBook.Book_cover1 = imageName;
            if(bookName!="") newBook.Book_cover2 = bookName;
            newBook.Book_countview = 0;
            newBook.Book_dateInsert = DateTime.Now.ToString();

            db.SaveChanges();


            return RedirectToAction("ViewBook");
        }

        public ActionResult DeleteBook(int id) {
            var item = db.tblBooks.FirstOrDefault(x => x.Book_id == id);
            db.tblBooks.Remove(item);
            db.SaveChanges();
            return RedirectToAction("ViewBook");
        }
        
        public ActionResult BookCategory()
        {
            var items = db.tblCategories.ToList();
            return View(items);
        }
        
        [HttpPost]
        public ActionResult AddCategory() {
            string name = Request.Form["category_name"];
            var category = new tblCategory();
            category.catName = name;
            db.tblCategories.Add(category);
            db.SaveChanges();

            return RedirectToAction("BookCategory");
        }

        public ActionResult DeleteCategory(int id) {
            var item = db.tblCategories.FirstOrDefault(x => x.catId == id);
            db.tblCategories.Remove(item);
            db.SaveChanges();
            return RedirectToAction("BookCategory");
        }

        public ActionResult Dashboard()
        {
            int books = db.tblBooks.Count();
            int categories = db.tblCategories.Count();
            ViewBag.Number = books;
            ViewBag.Category = categories;
            return View();
        }

        public ActionResult SlideShow()
        {
            var items = db.tblImages.ToList();
            return View(items);
        }

        [HttpPost]
        public ActionResult AddSlide()
        {
            var imageName = "";
            HttpPostedFileBase image = Request.Files["upload-file"];

            if (image.ContentLength > 0)
            {
                imageName = Path.GetFileName(image.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/Image"), imageName);
                image.SaveAs(path);
            }

            var slide = new tblImage();

            Guid guid = Guid.NewGuid();
            Random random = new Random();
            int i = random.Next();

            slide.imgPath=imageName;
            slide.imgId = i;
            db.tblImages.Add(slide);
            db.SaveChanges();
            return RedirectToAction("SlideShow");
        }

        public ActionResult DeleteSlide(int id) {
            var item = db.tblImages.FirstOrDefault(x => x.imgId == id);
            db.tblImages.Remove(item);
            db.SaveChanges();
            return RedirectToAction("SlideShow");
        }

    }
}
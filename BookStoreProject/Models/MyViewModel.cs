﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStoreProject.Models
{
    public class MyViewModel
    {
        public tblBook Book { get; set; }
        public tblCategory Category { get; set; }
        public List<tblCategory> Categories { get; set; }
    }
}